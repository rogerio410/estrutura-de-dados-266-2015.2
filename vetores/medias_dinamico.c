#include "stdio.h"
#include "stdlib.h"
float* calcular_media(float *n1, float *n2, int n);
int main(int argc, char const *argv[]){
	//Declaracao dinamica dos vetores
	float *notas_1bm = (float *) malloc(5 * sizeof(float));
	float *notas_2bm = (float *) malloc(5 * sizeof(float));
	
	//pedir ao usuario as notas de cada bimestre
	printf("Digite as notas dos alunos: \n"); 
	for (int i = 0; i < 5; ++i){
		printf("Digite Nota 1 e Nota2 do Aluno %d\n", i);
		scanf("%f %f", &notas_1bm[i], &notas_2bm[i]);
	}

	float *medias = calcular_media(notas_1bm, notas_2bm, 5);
	for (int i = 0; i < 5; ++i){
		printf("Aluno %d media %.2f\n", i, medias[i]);
	}

	free(notas_1bm);
	free(notas_2bm);
	free(medias);
	return 0;
}

float* calcular_media(float *n1, float *n2, int n){
	//criar vetor de médias dinam. e calcular as medias
	float *m = (float *)malloc(n * sizeof(float));

	//calcular as medias
	for (int i = 0; i < n; ++i){
		m[i] = (n1[i]+n2[i])/2;
	}
	return m;
}

