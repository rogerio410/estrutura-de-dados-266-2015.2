#include "stdio.h"
void dobra_valores(int *num, int n);

int main(int argc, char const *argv[])
{
	int numeros[10] = {1, 3, 4, 5, 6, 7, 7, 8, 8, 10};

	dobra_valores(numeros, 10);

	for (int i = 0; i < 10; ++i){
		printf("%d\n", numeros[i]);
	}
	

	return 0;
}

void dobra_valores(int *num, int n){
	for (int i = 0; i < n; ++i)
	{
		//num[i] = num[i]*2;
		num[i] = *(num+i) * 2;
	}
}