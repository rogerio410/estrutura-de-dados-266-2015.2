#include "stdio.h"

int main(void)
{
	float v[10];
	float med, var;

	for (int i = 0; i < 10; ++i){
		scanf("%f", &v[i]);
	}

	//for (int i = 0; i < 10; ++i){
	//	printf("Valor %d = %.1f\n",i, v[i] );
	//}

	//Calculo Media
	float soma = 0.0;
	for (int i = 0; i < 10; ++i){
		soma = soma + v[i];
	}

	med = soma/10.0;

	//Calculo Variancia
	soma = 0.0;
	for (int i = 0; i < 10; ++i){
		soma = soma + (v[i]-med)*(v[i]-med);
	}
	var = soma/10.0;

	printf("Media %.1f e Variancia %.1f\n", med, var);
	return 0;
}













