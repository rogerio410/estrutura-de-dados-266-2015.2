#include "stdio.h"
#include "stdlib.h"
void incrementar(int n, int *v){
	for (int i = 0; i < n; ++i){
		v[i] = v[i]*2;
	}
}

int main(int argc, char const *argv[])
{
	int *numeros = (int *)malloc(3*sizeof(int));
	for (int i = 0; i < 3; ++i){
		scanf("%d",&numeros[i]);
	}
	incrementar(3, numeros);

	for (int i = 0; i < 3; ++i){
		printf("item %d\n", numeros[i]);
	}
	free(numeros);
	return 0;
}