#include "stdio.h"

int main(int argc, char const *argv[])
{
	int valores[10];

	printf("Digite um valor: \n");
	scanf("%d", &valores[0]);

	for (int i = 1; i < 10; ++i){
		valores[i] = valores[i-1]*2;
	}

	for (int i = 0; i < 10; ++i){
		printf("%d ", valores[i]);
	}

	return 0;
}