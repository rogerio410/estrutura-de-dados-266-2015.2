#include "stdio.h"
#include "stdlib.h"
void calcular_media(float* n, int* f, int q, float* m);
int main(void){
	float* notas_1bm=(float*)malloc(10*sizeof(float));
	int* faltas=(int*) malloc(10*sizeof(int));

	//Receber os valores
	for (int i = 0; i < 10; ++i){
		printf("Digite Nota e Falta: ");
		scanf("%f %d", &notas_1bm[i], &faltas[i]);
	}

	//Calcular a media
	float* medias= (float *) malloc(10*sizeof(float));

	calcular_media(notas_1bm, faltas, 10, medias);

	//Imprimir
	for (int i = 0; i < 10; ++i){
		printf("Aluno %d media: %.2f\n",i, medias[i]);
	}

	free(notas_1bm);
	free(faltas);
	free(medias);
	return 0;
}

void calcular_media(float* n, int* f, int q, float* m){
	
	for (int i = 0; i < q; ++i){
		if (f[i] == 0){
			m[i] = n[i] + 2;
		}else if (f[i] <= 5){
			m[i] = n[i] + 1;
		}else{
			m[i] = n[i] + 0.5;
		}
	}
}



















