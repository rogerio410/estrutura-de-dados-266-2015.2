/*
	Uso do TAD Lista.
	Compilacao por partes, re-compilar apenas arqs que se modificaram
	>>> gcc -c lista.c -o lista.o
	>>> gcc -c exemplo_lista.c -o exemplo_lista.o
	>>> gcc -o prog_lista lista.o teste_lista.o
	>>> ./prog_lista

*/
#include "stdio.h"
#include "lista.h"

int main(){

	Lista lst;
	lst = nova_lista();
	int op = -1;
	while(op != 0){
		printf("LISTA:\n1-Novo\n2-Imprimir\n0-Sair\n" );
		scanf("%d", &op);
		if (op == 1){
			int vlr = 0;
			printf("Valor? ");
			scanf("%d", &vlr);
			inserir_final(&lst, vlr);
		}
		if (op == 2){
			imprimir_dados(lst);
		}
	}

	return 0;
}










