/*
	Definicao do TAD, Tipo e operações
*/

#define TAM_MAXIMO 10

struct lista{
	int inicio; //o indice do inicio
	int fim; //o indica do final
	int qtd_elementos; //qtd atual de elemtnos
	int dados[TAM_MAXIMO]; //sao os dados em si da lista
};

typedef struct lista Lista;

Lista nova_lista();
void inserir_final(Lista *l, int valor);
void inserir_inicio(Lista *l, int valor);
void inserir_posicao(Lista *l, int valor, int pos);
int remover_final(Lista *l);
int remover_inicio(Lista *l);
int remover_posicao(Lista *l);
void exibir_info(Lista l);
void imprimir_dados(Lista l);
int quantidade_elementos(Lista l);


