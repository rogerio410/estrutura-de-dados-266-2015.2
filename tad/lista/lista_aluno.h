/*
	Definicao do TAD, Tipo e operações
*/

#define TAM_MAXIMO 10

struct aluno{
	char nome[100];
	int matricula;
};

typedef struct aluno Aluno;

struct listaAluno{
	int inicio; //o indice do inicio
	int fim; //o indica do final
	int qtd_elementos; //qtd atual de elemtnos
	Aluno dados[TAM_MAXIMO]; //sao os dados em si da lista
};

typedef struct listaAluno Lista;

Lista nova_lista();
void inserir_final(Lista *l, Aluno a);
void inserir_inicio(Lista *l, Aluno a);
void inserir_posicao(Lista *l, Aluno a, int pos);
Aluno remover_final(Lista *l);
Aluno remover_inicio(Lista *l);
Aluno remover_posicao(Lista *l);
void exibir_info(Lista l);
void imprimir_dados(Lista l);
int quantidade_elementos(Lista l);


