/*
	Uso do TAD Lista.
	Compilacao por partes, re-compilar apenas arqs que se modificaram
	>>> gcc -c lista.c -o lista.o
	>>> gcc -c teste_lista.c -o teste_lista.o
	>>> gcc -o prog_lista lista.o teste_lista.o
	>>> ./prog_lista

*/
#include <stdio.h>
#include "lista.h"

//Protótipos
int menu();

int main(){
	
	//Váriavel do tipo lista
	Lista lista;
	//Criar, inicializa a lista
	lista = nova_lista();
	int valor = 0;
	int pos = 0;
	int r = 0;
	int opcao = -1;
	while (opcao != 0){
		opcao = menu();
		switch (opcao){
			case 1:	
				printf("Valor: ");
				scanf("%d", &valor);
				inserir_final(&lista, valor);
				break;
			case 2:
				exibir_info(lista);
				break;
			case 3:
				//Imprimir info e dados
				imprimir_dados(lista);
				break;
			case 4:
				printf("Valor: ");
				scanf("%d", &valor);
				inserir_inicio(&lista, valor);
				break;
			case 5:
				printf("Valor|Pos: ");
				scanf("%d %d", &valor, &pos);
				inserir_posicao(&lista, valor, pos);
				break;
			case 6:
				r = remover_final(&lista);
				printf("Elemento retirado da lista: %d\n", r);
				break;
		}
	}

	return 0;
}

int menu(){
	int op = 0;
	printf(">>>> Lista\n1 - Inserir Item no Fim\n2 - Info da Lista\n3 - Exibir Elementos\n4 - Inserir Item no Inicio\n5 - Inserir Item na Posicao\n6 - Remover no Final\n0 - Sair\n\n");
	scanf("%d", &op);
	return op;
}