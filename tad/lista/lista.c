/*
	Implementacao o TAD definido em lista.h
*/
#include "lista.h"
#include "stdio.h"

Lista nova_lista(){
	Lista lista;
	lista.inicio = 0;
	lista.fim = 0;
	lista.qtd_elementos = 0;
	return lista;
}

void inserir_final(Lista *l, int valor){
	//Por l ser ponteiro usa-se "->" inves do "."
	l->dados[l->fim] = valor;
	l->fim++;
	l->qtd_elementos++;
}

int quantidade_elementos(Lista l){
	return l.qtd_elementos;
}

void exibir_info(Lista l){
	printf("Lista, início: %d, fim: %d, Qtd Elementos: %d \n", l.inicio, l.fim, l.qtd_elementos);
}

void imprimir_dados(Lista l){
	exibir_info(l);
	printf("Elementos: \n");
	for (int i = l.inicio; i < l.fim; ++i){
		printf("\t%d --> %d\n", i, 	l.dados[i]);
	}
}

void inserir_inicio(Lista *l, int valor){
	//Deslocando
	for(int i = l->fim; i > l->inicio; i--){
		l->dados[i] = l->dados[i-1];
	}
	//Inserir
	l->dados[l->inicio] = valor;
	l->fim++;
	l->qtd_elementos++;

}

void inserir_posicao(Lista *l, int valor, int pos){
	//Deslocando
	for(int i = l->fim; i > pos; i--){
		l->dados[i] = l->dados[i-1];
	}
	//Inserir
	l->dados[pos] = valor;
	l->fim++;
	l->qtd_elementos++;
}

int remover_final(Lista *l){
	int removido = l->dados[l->fim-1];
	l->fim--;
	l->qtd_elementos--;
	return removido;
}


























