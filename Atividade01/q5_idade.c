#include <stdio.h>

int main(int argc, char const *argv[])
{
	int ano_nascimento;

	printf("Qual ano voce nasceu? \n");
	scanf("%d", &ano_nascimento);

	int idade_2035 = 2035 - ano_nascimento;
	printf("Em 2035 voce terah: %d anos\n", idade_2035);

	int idade_2016 = 2016 - ano_nascimento;
	printf("Em 2016 voce tem: %d anos\n", idade_2016);	

	if (idade_2016 >= 18){
		printf("\tVoce ja e maior de idade.\n");
	}

	return 0;
}