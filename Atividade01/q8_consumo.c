#include <stdio.h>

int main(int argc, char const *argv[])
{
	int desempenho = 12; //Km/l
	int tempo;
	int velocidade_media;
	int consumo_em_litros;

	printf("Quanto tempo (h)? \n");
	scanf("%d", &tempo);

	printf("Em qual velocidade media (km/h)?\n");
	scanf("%d", &velocidade_media);

	consumo_em_litros = (tempo * velocidade_media) / desempenho;

	printf("Voce gastou %d litros de combustível\n", consumo_em_litros);


	return 0;
}