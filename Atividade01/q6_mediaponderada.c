#include <stdio.h>

int main(int argc, char const *argv[])
{
	float nota1, nota2;

	printf("Digite a nota1: ");
	scanf("%f", &nota1);

	printf("Digite outra nota: ");
	scanf("%f", &nota2);

	float media_ponderada = (nota1*1.5 + nota2*1.9) / (1.5+1.9);

	printf("Media Ponderada %.4f\n", media_ponderada);

	return 0;
}





