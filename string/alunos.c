#include "stdio.h"
#include "string.h"

//Declaracao de Constante
#define MAX 3

//Variaveis Globais
char alunos[MAX][100];


void imprimir(char lista[MAX][100]){
	//Imprimindo nomes
	for (int i = 0; i < MAX; ++i){
		long tam = strlen(lista[i]);
		printf("Aluno %d - %s (TAM: %ld)\n", i, lista[i], tam);
	}

	//Forma correta de atribuir valor a um posicao vetor de string
	strcpy(lista[0],"Rogerio da Silva");
}

int main(int argc, char const *argv[]){

	//Ler nomes de alunos
	printf("Digite o nome de %d alunos: \n", MAX);
	for (int i = 0; i < MAX; ++i){
		//Leitura da linha: %[^\n] ou %100[^\n]
		scanf(" %[^\n]", alunos[i]);
	}

	imprimir(alunos);
	imprimir(alunos);
		
	return 0;
}