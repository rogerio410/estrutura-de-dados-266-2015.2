#include "stdio.h"
#include "string.h"

int main(int argc, char const *argv[])
{
	
	while(1){

		char cidade[100];
		printf("Cidade: ");
		scanf(" %100[^\n]", cidade);

		printf("\nCidade digitada %s\n", cidade);
		long tam = strlen(cidade);
		printf("Tamanho %ld\n", tam);
		printf("Primeiro e último: %c e %c\n", cidade[0], cidade[strlen(cidade)-1]);

		for (int i = 0; i < tam; ++i){
			printf("%c ", cidade[i]);
		}

		printf("\nCidade2: ");
		char cidade2[100];
		scanf(" %100[^\n]", cidade2);

		if (strcmp(cidade, cidade2) == 0){
			printf("Palavras Iguais\n");
		}else if (strcmp(cidade, cidade2) > 0){
			printf("%s maior que %s\n", cidade, cidade2);
		}else{
			printf("%s menor que %s\n", cidade, cidade2);
		}
	}
	return 0;
}