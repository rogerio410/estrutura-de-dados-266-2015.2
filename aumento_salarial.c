#include <stdio.h>

int main(int argc, char const *argv[])
{
	float salario;
	printf("Digite o salario: ");
	scanf("%f", &salario);

	double novo_salario;
	int taxa_aumento;

	if (salario <= 400){
		novo_salario = salario*1.15;
		taxa_aumento = 15;
	}else if (salario <= 800){
		novo_salario = salario*1.12;
	}

	float valor_aumento = novo_salario - salario;

	printf("Novo Salario %.2f\n", novo_salario);
	printf("Reajuste ganho %.2f\n", valor_aumento);
	printf("Em percentual %d %%\n ", taxa_aumento);

	return 0;
}








