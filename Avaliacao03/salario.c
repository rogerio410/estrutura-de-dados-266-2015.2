#include "stdio.h"
float calc_sal(float s, float v);

int main(){
	float salario;
	float vendas;
	float novo_salario;
	printf("Salario: \n");
	scanf("%f", &salario);
	printf("Vendas: \n");
	scanf("%f", &vendas);

	novo_salario = calc_sal(salario, vendas);

	printf("Novo Salario: R$ %.2f\n", novo_salario);

	return 0;
}

float calc_sal(float s, float v){
	float ns;
	ns = v*0.15 + s;
	return ns;
}