#include "stdio.h"
void quad(int x, int y, int *q);
int main(){
	
	int x=-1, y=-1, quadrante;

	while(x!=0 || y!=0){
		printf("Digite X Y: ");
		scanf("%d %d", &x, &y);

		quad(x, y, &quadrante);

		if (quadrante == 1) printf("primeiro\n");
		if (quadrante == 2) printf("segundo\n");
		if (quadrante == 3) printf("terceiro\n");
		if (quadrante == 4) printf("quarto\n");
	}

	return 0;
}

void quad(int x, int y, int *q){
		if (x>0 && y>0) *q = 1;
		if (x<0 && y>0) *q = 2;
		if (x<0 && y<0) *q = 3;
		if (x>0 && y<0) *q = 4;
}




