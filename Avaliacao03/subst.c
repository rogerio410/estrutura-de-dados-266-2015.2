#include "stdio.h"
int substituir(int v);
int main(){
	int numeros[10];

	printf("Digite 10 valores: \n");
	for (int i = 0; i < 10; ++i){
		scanf("%d", &numeros[i]);	
	}

	for (int i = 0; i < 10; ++i){
		numeros[i] = substituir(numeros[i]);
		printf("%d ", numeros[i]);
	}

	return 0;
}

int substituir(int v){
	if (v <= 0){
		return 1;
	}else{
		return v;
	}
}