#include "stdio.h"
int main(){
	
	int n;
	printf("Digite um valor: ");
	scanf("%d", &n);
	
	int numeros[n*4];
	
	for (int i = 0; i < n*4; ++i){
		numeros[i] = i+1;
	}

	for (int i = 0; i < (n*4); ++i){
		if (numeros[i] % 4 == 0){
			printf("IFPI \n");
		}else{
			printf("%d ", numeros[i]);
		}
	}

	printf("\n");
	
	return 0;
}