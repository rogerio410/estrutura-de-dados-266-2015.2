#include <stdio.h>

int main(int argc, char const *argv[])
{
	int cont_par = 0;
	int cont_impar = 0;
	int cont_pos = 0;
	int cont_neg = 0;

	for (int i = 1; i <= 5; ++i){
		int numero;
		printf("Digite o %do.numero: \n", i;
		scanf("%d", &numero);
		
		//Se positivo ou negativo
		if (numero > 0){
			cont_pos++;
		}else if (numero < 0){
			cont_neg++;
		}

		//Se impar ou par
		if (numero % 2 == 0){
			cont_par++;
		}else{
			cont_impar++;
		}
	}

	printf("%d valor(es) par(es)\n", cont_par);
	printf("%d valor(es) impar(es)\n", cont_impar);
	printf("%d valor(es) positivo(s)\n", cont_pos);
	printf("%d valor(es) negativo(s)\n", cont_neg);

	return 0;














}