//fazer um programa com duas variaveis e chamar uma 
//funcao que incremente as variaiveis um valor tambem
//enviado
#include "stdio.h"
void incrementar(int *v1, int *v2, int *v3, int n);

int main(int argc, char const *argv[])
{
	int a, b, c, n;

	printf("Digite tres valores: ");
	scanf("%d %d %d", &a, &b, &c);

	n = 4;
	incrementar(&a, &b, &c, n);

	printf("%d %d %d\n", a, b, c);

	return 0;
}

void incrementar(int *v1, int *v2, int *v3, int n){
	*v1 += n; // *v1 = *v1 + n
	*v2 += n;
	*v3 += n; 
}








