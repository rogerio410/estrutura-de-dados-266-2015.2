#include "stdio.h"
void troca(int *px, int *py);

int main(int argc, char const *argv[])
{
	int a, b;
	a = 5;
	b = 7;
	printf("%d %d\n", a, b);
	troca(&a, &b);
	printf("%d %d\n", a, b);
	return 0;
}

void troca(int *px, int *py){
	int temp;
	temp = *px;
	*px = *py;
	*py = temp;
}