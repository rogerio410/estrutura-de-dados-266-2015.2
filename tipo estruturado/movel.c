#include "movel.h"
#include "string.h"

typedef struct movel Movel;

int qtdLetrasNoNome(Movel v){
	int qtd = strlen(v.nome);
	return qtd;
}

float qtdTintaParaPintar(Movel v){
	int qtd_tinta = (v.larg*v.alt)*2;
	return qtd_tinta;
}
