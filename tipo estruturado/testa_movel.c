#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "movel.h"

/*
	Compilacao:
	>>> gcc -c movel.c -o movel.o
	>>> gcc -c testa_movel.c -o testa_movel.o
	>>> gcc -o prog_movel testa_movel.o movel.o
	Executar:
	>>> ./prog_movel
*/

int main(){
	Movel mesa;
	
	//Atribuir valor a uma string
	strcpy(mesa.nome, "Mesa do Jose");
	
	mesa.larg = 100.5;
	mesa.alt = 65.6;

	int qtdLetras = qtdLetrasNoNome(mesa);

	printf(">>> %s tem %d letras \n", mesa.nome, qtdLetras);

	return 0;
}