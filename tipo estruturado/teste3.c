#include "stdio.h"
struct ponto{
	float x;
	float y;
};

void imprime(struct ponto p);
int main(){
	struct ponto p;
	printf("Digite x e y: ");
	scanf("%f %f", &p.x, &p.y);
	imprime(p);	
}

void imprime(struct ponto p){
	printf("Ponto: (%.1f, %.1f)\n", p.x, p.y);
}