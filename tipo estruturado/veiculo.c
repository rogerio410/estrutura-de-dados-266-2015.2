#include "stdio.h"
#include "stdlib.h"

struct veiculo{
	char nome[50];
	char montadora[50];
	int ano_fabricacao;
	int ano_modelo;
	char cor[30];
	char placa[8];
	float valor;
};

typedef struct veiculo Carro;
void imprimir_carro();
void leitura_carro();

Carro car;

int main(){
	
	leitura_carro();
	imprimir_carro();
	
	return 0;
}

void leitura_carro(){
	printf("Digite os dados do veículo:\n");
	printf("Nome: ");
	scanf(" %[^\n]", car.nome);
	printf("Ano Fab./Ano Modelo: ");
	scanf("%d %d", &car.ano_fabricacao, &car.ano_modelo);
	printf("Cor: ");
	scanf(" %[^\n]", car.cor);
	printf("Valor: ");
	scanf("%f", &car.valor);
}

void imprimir_carro(){
	printf("\n\nCarro: %s - %d, cor: %s, R$: %.2f\n\n",car.nome, car.ano_modelo, car.cor, car.valor );
}












