#include "stdio.h"
#include "math.h"
struct ponto{
	float x;
	float y;
};
void captura(struct ponto *pp);
float distancia(struct ponto *p, struct ponto *q);

int main(){
	struct ponto p, q;
	captura(&p);
	captura(&q);
	float dist = distancia(&p, &q);
	printf("Distancia entre os pontos: %.2f \n", dist);	
	return 0;
}

float distancia(struct ponto *p, struct ponto *q){
	float step1 = (q->x	- p->x)*(q->x - p->x);
	float step2 = (q->y - p->y)*(q->y - p->y);
	float step3 = sqrt(step1+step2);
	return step3;
}

void captura(struct ponto *pp){
	printf("Digite x e y: ");
	scanf("%f %f", &pp->x, &pp->y);
};
	
