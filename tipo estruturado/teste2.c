#include "stdio.h"
#include "stdlib.h"

struct ponto{
	float x;
	float y;
};

int main(){
	struct ponto *p = (struct ponto *)malloc(sizeof(struct ponto));
	printf("Digite x e y: ");
	scanf("%f %f", &p->x, &p->y);
	printf("Ponto: (%.1f, %.1f)\n", p->x, p->y);
}