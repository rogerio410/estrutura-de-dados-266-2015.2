#include "stdio.h"
#include "math.h"
//Estrutura (geralmente primeira letra e minusculos)
struct ponto{
	float x;
	float y;
};
//Definindo um novo Tipo baseado na Estrutura Ponto
//Geralmente primeira letra em Maiusculo
typedef struct ponto Ponto;

void captura(Ponto *pp);
float distancia(Ponto *p, Ponto *q);

int main(){
	Ponto p, q;
	captura(&p);
	captura(&q);
	float dist = distancia(&p, &q);
	printf("Distancia entre os pontos: %.2f \n", dist);	
	return 0;
}

float distancia(Ponto *p, Ponto *q){
	float step1 = (q->x	- p->x)*(q->x - p->x);
	float step2 = (q->y - p->y)*(q->y - p->y);
	float step3 = sqrt(step1+step2);
	return step3;
}

void captura(Ponto *pp){
	printf("Digite x e y: ");
	scanf("%f %f", &pp->x, &pp->y);
};
	
