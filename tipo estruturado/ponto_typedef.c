#include "stdio.h"

struct ponto{
	float x;
	float y;
};

typedef struct ponto Ponto;

void imprimir(Ponto ray);
void leitura(Ponto *pp);

int main(void){
	Ponto p;
	leitura(&p);
	imprimir(p);
	return 0;
}

void leitura(Ponto *pp){
	printf("Digite Ponto X Y:");
	scanf("%f %f", &pp->x, &pp->y);
}

void imprimir(Ponto ray){
	printf("Ponto digitado:(%.1f, %.1f)\n",ray.x, ray.y );
}







