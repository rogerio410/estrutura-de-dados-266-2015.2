#include "stdio.h"
#include "stdlib.h"

struct ponto{
	float x;
	float y;
};
void imprimir(struct ponto *ray);
void leitura(struct ponto *pp);

int main(void){
	struct ponto *p;
	p = (struct ponto*)malloc(sizeof(struct ponto));
	leitura(p);
	imprimir(p);
	free(p);
	return 0;
}

void leitura(struct ponto *pp){
	printf("Digite Ponto X Y:");
	scanf("%f %f", &pp->x, &pp->y);
}

void imprimir(struct ponto *ray){
	printf("Ponto digitado:(%.1f, %.1f)\n",ray->x, ray->y );
}







