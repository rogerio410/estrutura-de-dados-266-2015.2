#include "stdio.h"
#include "stdlib.h"

#define QTD 10

//Definicao do Tipo Carro
struct veiculo{
	char nome[50];
	char montadora[50];
	int ano_fabricacao;
	int ano_modelo;
	char cor[30];
	char placa[8];
	float valor;
};

typedef struct veiculo Carro;

void inicializa();
void novo_carro(int p);
void imprimir_carro(int p);

//Vetores de Carros
Carro* carros[QTD];

int main(){
	inicializa();
	novo_carro(3);
	imprimir_carro(3);
	return 0;
}

void inicializa(){
	for (int i = 0; i < QTD; ++i){
		carros[i] = NULL;
	}
}

void novo_carro(int p){

	if (p < 0 || p >= QTD){
		printf("Posicao Invalida\n");
		exit(1);
	}
	//Criar espaco em memoria para o novo carro.
	carros[p] = (Carro *)malloc(sizeof(Carro));
	//Receber os dados
	printf("Digite os dados do veículo:\n");
	printf("Nome: ");
	scanf(" %[^\n]", carros[p]->nome);
	printf("Ano Fab./Ano Modelo: ");
	scanf("%d %d", &carros[p]->ano_fabricacao, &carros[p]->ano_modelo);
	printf("Cor: ");
	scanf(" %[^\n]", carros[p]->cor);
	printf("Valor: ");
	scanf("%f", &carros[p]->valor);
	printf("\nVeiculo inserido com sucesso !!!\n");

}

void imprimir_carro(int p){
	if (carros[p] != NULL){
		printf("\nCarro %d: %s - %d, cor: %s, R$: %.2f", p, carros[p]->nome, carros[p]->ano_modelo, carros[p]->cor, carros[p]->valor );
	}
}




















