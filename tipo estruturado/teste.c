#include "stdio.h"

struct ponto{
	float x;
	float y;
};

int main(){
	struct ponto p;
	printf("Digite x e y: ");
	scanf("%f %f", &p.x, &p.y);
	printf("Ponto: (%.1f, %.1f)\n", p.x, p.y);
}