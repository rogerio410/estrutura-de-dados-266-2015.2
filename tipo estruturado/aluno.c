#include "stdio.h"
#include "stdlib.h"

struct aluno{
	char nome[100];
	int matricula;
	char endereco[100];
	char telefone[14];
};

typedef struct aluno Aluno;

int main(){
	Aluno a;

	printf("Digite os dados do aluno.\n");
	printf("Nome: ");
	scanf(" %[^\n]", a.nome);
	printf("Matricula: ");
	scanf("%d", &a.matricula);
	printf("Endereco: ");
	scanf(" %[^\n]", a.endereco);
	printf("Telefone: ");
	scanf(" %[^\n]", a.telefone);

	printf("\nAluno: %s e %s\n", a.nome, a.telefone);
	return 0;
}








