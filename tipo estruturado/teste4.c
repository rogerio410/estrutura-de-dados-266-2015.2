#include "stdio.h"
struct ponto{
	float x;
	float y;
};

void imprime(struct ponto p);
void captura(struct ponto *pp);

int main(){
	struct ponto p;
	captura(&p);
	imprime(p);	
	return 0;
}

void captura(struct ponto *pp){
	printf("Digite x e y: ");
	scanf("%f %f", &pp->x, &pp->y);
};

void imprime(struct ponto p){
	printf("Ponto: (%.1f, %.1f)\n", p.x, p.y);
}