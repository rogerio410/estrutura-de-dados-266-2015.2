#include "stdio.h"

#define QUANTIDADE 10

struct aluno{
	int matricula;
	char nome[100];
	char curso[100];
	char sexo[1];
	float renda;	
};

typedef struct aluno Aluno;

//Variáveis Globais.
Aluno alunos[QUANTIDADE];
int posicao = 0;

//Prototipos
void inicializar();
void novo_aluno();
void exibir_aluno(int p);
void exibir_alunos();
void exibir_por_matricula();

//Principal
int main(){
	inicializar();

	int opcao = -1;
	while(opcao != 0){
		printf("**** SisALUNOS ****\n1 - Novo\n2 - Exibir\n3 - Listar Todos\n4 - Quantidade\n5 - Exibir por Matricula\n0 - Sair\n\n Digite uma opcao: ");
		scanf("%d", &opcao);

		if (opcao == 1) 
			novo_aluno();

		if (opcao == 2){
			int p;
			printf("Qual? ");
			scanf("%d", &p);
			exibir_aluno(p);
		}

		if (opcao == 3){
			exibir_alunos();
		}

		if (opcao == 4){
			printf("Quantidade Cadastrados: %d\n", posicao );
		}

		if (opcao == 5){
			exibir_por_matricula();
		}
	}
}

//Funcoes que o Principal Usa.

void inicializar(){
	for (int i = 0; i < QUANTIDADE; ++i){
		alunos[i].matricula = -1;
	}
}

void novo_aluno(){
	printf("Digite os dados do aluno.\n");
	printf("Matricula: ");
	scanf("%d", &alunos[posicao].matricula);
	printf("Nome: ");
	scanf(" %[^\n]", alunos[posicao].nome);
	printf("Curso: ");
	scanf(" %[^\n]", alunos[posicao].curso);
	printf("Sexo(M|F): ");
	scanf(" %[^\n]", alunos[posicao].sexo);
	printf("Renda(R$): ");
	scanf("%f", &alunos[posicao].renda);
	posicao = posicao + 1;
}

void exibir_aluno(int p){
	if (alunos[p].matricula != -1){
		printf("Aluno : %d, %s, %s, %s, %.2f \n", alunos[p].matricula, alunos[p].nome, alunos[p].curso, alunos[p].sexo, alunos[p].renda);
	}
}

void exibir_alunos(){
	for (int i = 0; i < QUANTIDADE; ++i){
		exibir_aluno(i);
	}
}

void exibir_por_matricula(){
	int matricula;
	printf("Matricula: ");
	scanf("%d", &matricula);
	for (int i = 0; i < QUANTIDADE; ++i){
		if (alunos[i].matricula == matricula){
			exibir_aluno(i);
		}
	}
}













