#include "stdio.h"
#define MAX 10 //definicao de constante(ATENCAO.. sem ; ao final)
//Estrutura aluno
struct aluno{
	char nome[80];
	int matricula;
	char endereco[100];
	char telefone[15];
};
//Tipo de dado Aluno a partir de aluno
typedef struct aluno Aluno;

//Vetor de alunos
Aluno alunos[MAX];
Aluno* alunos2[MAX];

//Prototipos
void inicializar();
void novo_aluno(int i);
void remover_aluno(int i);
void imprimir_aluno(int i);
void imprimir_todos();

int main(void){
	inicializar(); //alunos[0].matricula = -1;
	
	if (alunos[0].matricula == -1){
		printf("Aluno 0 is null!\n");
	}

	novo_aluno(0);
	imprimir_aluno(0);
	novo_aluno(1);
	imprimir_todos();
	remover_aluno(0);
	imprimir_todos();

	return 0;
}

void inicializar(){
	for (int i = 0; i < MAX; i++){
		alunos[i].matricula = -1;
	}
}

void novo_aluno(int i){
	printf("Entre com o nome:");
	scanf(" %80[^\n]", alunos[i].nome); //nome e vetor, portanto nao precisa do & no scanf
	printf("Entre com a matricula:");
	scanf("%d", &alunos[i].matricula); //Somente este precisa do &
	printf("Entre com o endereco:");
	scanf(" %120[^\n]", alunos[i].endereco);
	printf("Entre com o telefone:");
	scanf(" %20[^\n]", alunos[i].telefone);
}

void remover_aluno(int i){
	alunos[i].matricula = -1;
}

void imprimir_aluno(int i){
	if (alunos[i].matricula != -1){
		printf("Aluno = %d\n", i);
		printf("Nome: %s\n", alunos[i].nome);
		printf("Matrícula: %d\n", alunos[i].matricula);
		printf("Endereço: %s\n", alunos[i].endereco);
		printf("Telefone: %s\n", alunos[i].telefone);
	}
}

void imprimir_todos(){
	for (int i = 0; i < MAX; i++){
		imprimir_aluno(i);
	}
}










