#include "stdio.h"
#include "stdlib.h"

#define QTD 90000000

//Definicao do Tipo Carro
struct veiculo{
	char nome[50];
	char montadora[50];
	int ano_fabricacao;
	int ano_modelo;
	char cor[30];
	char placa[8];
	float valor;
};

typedef struct veiculo Carro;

//Prototipos das funcoes
void imprimir_carro(int i);
void inserir_carro();
void inicializar();
void imprimir_todos();


//Vetor de carros
Carro carros[QTD];

//Posicao atual de insercao
int pos = 0; 

int main(){
	
	inicializar();

	//Menu de opcoes
	int op = -1;
	while(op != 0){
		 printf("\nSistema de Veículo\n\n1 - Novo Carro\n2 - Mostrar\n3 - Mostrar Todos\n0 - Sair\n\nDigite uma opcao: ");
		 scanf("%d", &op);

		 if (op == 1){ //Novo Carro
		 	inserir_carro();
		 }

		 if (op == 2){ //Mostrar um veiculo
		 	int p;
		 	printf("Qual posicao? ");
		 	scanf("%d", &p);
		 	imprimir_carro(p);
		 }

		 if (op == 3){
		 	imprimir_todos(carros);
		 }

	}

	return 0;
}

void inicializar(){
	for (int i = 0; i < QTD; ++i){
		carros[i].ano_modelo = -1;
	}
	printf("Sistema inicializado com sucesso!!!\n");
}

void inserir_carro(){
	printf("Digite os dados do veículo:\n");
	printf("Nome: ");
	scanf(" %[^\n]", carros[pos].nome);
	printf("Ano Fab./Ano Modelo: ");
	scanf("%d %d", &carros[pos].ano_fabricacao, &carros[pos].ano_modelo);
	printf("Cor: ");
	scanf(" %[^\n]", carros[pos].cor);
	printf("Valor: ");
	scanf("%f", &carros[pos].valor);
	printf("\nVeiculo inserido com sucesso !!!\n");
	//incrementar a posicao para inserir
	pos = pos + 1; 
}

void imprimir_carro(int i){
	//tem um carro nessa posicao?
	if (carros[i].ano_modelo == -1){
		//printf("Carro inexistente!\n");
	}else{
		printf("\nCarro %d: %s - %d, cor: %s, R$: %.2f", i, carros[i].nome, carros[i].ano_modelo, carros[i].cor, carros[i].valor );
	}	
}

void imprimir_todos(){
	printf("\nTODOS OS CARROS CADASTRADOS.\n");
	for (int i = 0; i < QTD; ++i){
		imprimir_carro(i);
	}
}












