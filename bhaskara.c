#include <stdio.h>
#include <math.h>

float calcular_delta(float a, float b, float c);
float raiz_r1(float a, float b, float d);
float raiz_r2(float a, float b, float d);

int main(void){
	float a, b, c, delta;
	float r1, r2;

	printf("Digite A B C:\n");
	scanf("%f %f %f", &a, &b, &c);

	delta = calcular_delta(a, b, c);
	//calcular_delta(a, b, c, &delta);


	if (delta < 0 || a <= 0){
		printf("Impossível calcular.\n");
	}else{
		r1 = raiz_r1(a, b, delta);
		r2 = raiz_r2(a, b, delta);

		printf("R1: %.5f \nR2: %.5f\n", r1, r2);
	}
	

	return 0;
}

float calcular_delta(float a, float b, float c){
	float delta = pow(b,2) - 4*a*c;
	//float delta = b*b - 4*a*c;
	return delta;
}

float raiz_r1(float a, float b, float d){
	float r1;
	r1 = (-1*b + sqrt(d))/(2*a);
	return r1;
}

float raiz_r2(float a, float b, float d){
	float r2;
	r2 = (-1*b - sqrt(d))/(2*a);
	return r2;
}

int funcao2(int numero){
	numero = numero * 2;
	return numero;
}













