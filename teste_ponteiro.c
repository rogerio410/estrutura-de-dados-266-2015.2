#include "stdio.h"

void dobrar(int *v){
	*v = *v * 2;
}

int main(void){
	
	int v = 10;
	dobrar(&v);
	printf("%d\n", v);
	return 0;
}