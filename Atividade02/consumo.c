#include "stdio.h"
void consumo(float desemp, int dist, float *litros);
int main(){

	int distancia; 
	float desempenho;
	float litros;

	printf("Qual o desempenho(km/l) do seu veiculo: ");
	scanf("%f", &desempenho);

	printf("Qual a distancia(km) da sua viagem: ");
	scanf("%d", &distancia);

	consumo(desempenho, distancia, &litros);
	printf("Voce precisara de %.1f litros de combus.\n", litros);
	return 0;
}

void consumo(float desemp, int dist, float *litros){
	*litros = dist/desemp;
}



