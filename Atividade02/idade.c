#include "stdio.h"
void calcula_idade(int ano1, int ano2, int *p);
int main(){

	int ano_nascimento, ano_futuro, idade;

	printf("Digite o ano nascimento: \n");
	scanf("%d", &ano_nascimento);

	printf("Digite um ano futuro: \n");
	scanf("%d", &ano_futuro);

	//Validando o ano futuro
	while(ano_futuro < ano_nascimento){
		printf("Digite, de fato, um ano futuro: \n");
		scanf("%d", &ano_futuro);
	}

	calcula_idade(ano_nascimento, ano_futuro, &idade);

	printf("Idade %d\n", idade);

	if (idade < 12){
		printf("Crianca\n" );
	}else if (idade < 24){ // >= 12
		printf("Jovem\n");
	}else if (idade < 65){
		printf("Adulto\n");
	}else{
		printf("Idoso\n");
	}
	
	return 0;
}

void calcula_idade(int ano1, int ano2, int *p){
	*p = ano2 - ano1;
}








