#include "stdio.h"

int main(void){
	
	int x, y;
	int somatorio = 0;

	printf("Digite dois valores(X e Y): \n");
	scanf("%d %d", &x, &y);

	while (y < x){
		printf("Digite Y novamente. Maior que X:\n");
		scanf("%d", &y);
	}

	for(int i=x; i <= y; i++){
		if (i % 13 != 0){
			printf("%d\n", i);
			somatorio = somatorio + i;
		}
	}

	printf("somatorio nao multiplus(13): %d\n", somatorio);

	return 0;
}
