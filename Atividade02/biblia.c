#include "stdio.h"

int main(int argc, char const *argv[])
{
	int n;

	printf("Quantos fatos da história: \n");
	scanf("%d", &n);

	for (int i = 0; i < n; ++i){
		int qtd_anos;
		int ano;
		printf("Fato %d, ha quantos anos? ", (i+1));
		scanf("%d", &qtd_anos);

		ano = 2016 - qtd_anos;

		if (ano > 0){
			printf("%d D.C\n", ano);
		}else{
			printf("%d A.C\n", (ano*-1)+1);
		}

	}

	return 0;
}