#include "stdio.h"
int fatorial(int n);

int main(void){
	int n, k, arranjos;
	printf("Digite Qtd de Elemento e Tam Arranjos\n");
	scanf("%d %d", &n, &k);

	arranjos = fatorial(n) / fatorial(n - k);
	printf("Qtd de Arranjos: %d\n", arranjos);

	return 0;
}

int fatorial(int n){
	int f = 1;
	for (int i = 1; i <= n; ++i)
	{
		f *= i; // f = f * 1
	}
	return f;
}